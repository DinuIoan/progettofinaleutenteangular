import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { UtenteBaseImpostazioniComponent } from './utente-base-impostazioni/utente-base-impostazioni.component';
import { UtenteBaseComponent } from './utente-base/utente-base.component';

const routes: Routes = [
  {path:"", redirectTo:"utente/login", pathMatch:"full"},
  {path:"utente/login", component:LoginComponent},
  {path:"utente/base", component:UtenteBaseComponent},
  {path:"utente/base/impostazioni", component:UtenteBaseImpostazioniComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
