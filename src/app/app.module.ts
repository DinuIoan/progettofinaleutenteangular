import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {HttpClient, HttpClientModule, HttpHandler} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { UtenteBaseComponent } from './utente-base/utente-base.component';
import { UtenteBaseImpostazioniComponent } from './utente-base-impostazioni/utente-base-impostazioni.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UtenteBaseComponent,
    UtenteBaseImpostazioniComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
