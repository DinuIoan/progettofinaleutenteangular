import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-utente-base-impostazioni',
  templateUrl: './utente-base-impostazioni.component.html',
  styleUrls: ['./utente-base-impostazioni.component.css']
})
export class UtenteBaseImpostazioniComponent implements OnInit {

  inputUsername:String;
  inputPassword:String;

  constructor() { }

  ngOnInit(): void {
  }

  logOutUtente(){
    
  }

  modificaUtente(){

  }

  eliminaUtente(){
    
  }

}
