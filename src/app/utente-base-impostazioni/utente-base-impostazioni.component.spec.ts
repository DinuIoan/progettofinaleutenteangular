import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtenteBaseImpostazioniComponent } from './utente-base-impostazioni.component';

describe('UtenteBaseImpostazioniComponent', () => {
  let component: UtenteBaseImpostazioniComponent;
  let fixture: ComponentFixture<UtenteBaseImpostazioniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtenteBaseImpostazioniComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtenteBaseImpostazioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
