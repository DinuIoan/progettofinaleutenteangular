import { Prodotto } from "./prodotto";

export class Store {
    idStore:        number | undefined;
    nomeS:          String | undefined;
    indirizzo:      String | undefined;
    elencoProdotti: Prodotto[] | undefined;
}
