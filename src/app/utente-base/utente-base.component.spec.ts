import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtenteBaseComponent } from './utente-base.component';

describe('UtenteBaseComponent', () => {
  let component: UtenteBaseComponent;
  let fixture: ComponentFixture<UtenteBaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtenteBaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtenteBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
