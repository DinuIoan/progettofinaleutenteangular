export class Prodotto {
    idProdotto:          number | undefined;
    codiceP:             String | undefined;
    nomeP:               String | undefined;
    prezzo:              number | undefined;
    quantitaDisponibile: number | undefined;
}
