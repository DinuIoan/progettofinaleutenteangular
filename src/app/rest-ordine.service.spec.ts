import { TestBed } from '@angular/core/testing';

import { RestOrdineService } from './rest-ordine.service';

describe('RestOrdineService', () => {
  let service: RestOrdineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RestOrdineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
