import { Prodotto } from "./prodotto";

export class Ordine {
    codiceUtente:   number | undefined;
    totale:         number | undefined;
    dataOrdine:     Date | undefined;
    elencoProdotti: Prodotto[] | undefined;
}
