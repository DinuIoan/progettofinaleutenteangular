import { TestBed } from '@angular/core/testing';

import { RestUtenteService } from './rest-utente.service';

describe('RestUtenteService', () => {
  let service: RestUtenteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RestUtenteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
