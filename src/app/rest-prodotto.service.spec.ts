import { TestBed } from '@angular/core/testing';

import { RestProdottoService } from './rest-prodotto.service';

describe('RestProdottoService', () => {
  let service: RestProdottoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RestProdottoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
