export class Utente {
    idUtente:   number | undefined;
    usernameU:  String | undefined;
    paswU:      String | undefined;
    tipologia:  String | undefined;
    indirizzo:  String | undefined;
}
